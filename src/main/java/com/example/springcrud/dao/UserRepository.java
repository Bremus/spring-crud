package com.example.springcrud.dao;

import com.example.springcrud.dto.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,Long> {


}