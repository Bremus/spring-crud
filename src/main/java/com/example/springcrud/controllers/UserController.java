package com.example.springcrud.controllers;

import com.example.springcrud.dao.UserRepository;
import com.example.springcrud.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class UserController {

    @Autowired
    UserRepository userRepository;

    @GetMapping("/") // "/" reprezinta radacina aplicatiei tale
    public String getAllUsers(Model model) {
        final Iterable<User> allUsers = userRepository.findAll();
        model.addAttribute("totiUtilizatorii", allUsers);
        return "all-users";
    }

    @GetMapping("/user.html")
    public String goToEditPage(@ModelAttribute User user) {
        return "user";
    }

    @PostMapping("/saveUser")
    public String saveUser(@Valid User user, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult.getAllErrors());
            return "user";
        } else {
            System.out.println(user);
            userRepository.save(user);
            return "redirect:/";
        }
    }

    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Nu am gasit Userul cu id-ul " + id));
        userRepository.delete(user);
        return "redirect:/";
    }

    @GetMapping("/update/{id}")
    public String goToEditPage(@PathVariable Long id, Model model) {
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Nu am gasit Userul cu id-ul " + id));
        model.addAttribute("user", user);
        return "user";
    }
    
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setDisallowedFields("user.id");
    }
}