package com.example.springcrud.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor

@Entity(name = "Users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Acest camp este obligatoriu")
    @Column
    private String nume;

    @Pattern(regexp = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$", message = "Formatul numarului de tel este invalid")
    @NotBlank(message = "Acest camp este obligatoriu")
    @Column(nullable = false) //acest camp este obligatoriu de introdus
    private String telefon;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "data_nasterii")
    private Date dataNasterii;
}